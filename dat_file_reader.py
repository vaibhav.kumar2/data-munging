class Filereader:
    '''
    This class takes data file
    '''
    def __init__(self, file):
        self.file = file

    def getting_content(self, key_value, max_value, min_value):
        '''
        Read data from File and return list of list
        '''
        with open(self.file, 'r') as datfile:
            columns_data = []
            line_reader = datfile.readline()
            while line_reader != "":
                line_reader = datfile.readline()
                columns = line_reader.split()
                if columns:
                    if len(columns) <= 1:
                        continue
                    columns_data.append([columns[key_value],\
                                         columns[max_value], columns[min_value]])
        return columns_data
