'''
For running the files
'''
from dat_file_reader import Filereader
from data_cleaner import Datacleaner
from calculation_difference import Calculation

def display_min_temperature_day(weatherfile):
    ''''
    Display the minimum temperature in weather
    '''
    weather_reader = Filereader(weatherfile)
    weather_columns = weather_reader.getting_content(0, 1, 2)
    columns_cleaner = Datacleaner().weather_data_cleaner(weather_columns)
    calculate_differ = Calculation().calculate_difference(columns_cleaner)
    return calculate_differ


def display_min_goal_differ(footballfile):
    '''
    Display the minimum temperature in weather
    '''
    football_reader = Filereader(footballfile)
    football_columns = football_reader.getting_content(1, 6, 8)
    calculate_differ = Calculation().calculate_difference(football_columns)
    return calculate_differ


print(display_min_temperature_day('weather.dat'))
print(display_min_goal_differ('football.dat'))
