class Calculation:
    '''
    This class take the columns for calculation
    '''
    def calculate_difference(self,columns_data):
        ''''
        Calculate the difference
        '''
        minimum_diff = 99
        for col in columns_data:
            column_differ = abs(int(col[1]) - int(col[2]))
            if column_differ < minimum_diff:
                minimum_diff = column_differ
                index_of_min = col[0]
        return index_of_min, minimum_diff
