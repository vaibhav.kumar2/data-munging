def football_calculation(footballfile):
    minimum_diff = 99
    with open(footballfile, 'r') as football:
        football_reader = football.readline()
        while football_reader != "":
            football_reader = football.readline()
            football_columns = football_reader.split()
            if len(football_columns) <= 1:
                continue
            if football_columns:
                goal_diff = abs(int(football_columns[2]) - int(football_columns[3]))
                if goal_diff > minimum_diff:
                    continue
                else:
                    minimum_diff = goal_diff
                    index_of_min_goal = football_columns[0]
                    team = football_columns[1]

        print(index_of_min_goal, team)
football_calculation('football.dat')
