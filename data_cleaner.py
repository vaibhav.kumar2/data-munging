class Datacleaner:
    '''
    This class take the data and extra data
    '''
    def weather_data_cleaner(self, weather_columns):
        '''
        Clean superfluous data
        '''
        for col in weather_columns:
            col[1] = col[1].replace("*", "")[:2]
            col[2] = col[2].replace("*", "")[:2]
        return weather_columns
