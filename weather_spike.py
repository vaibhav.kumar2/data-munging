def weather_calculation(weatherfile):
    minimum_diff = 99
    with open(weatherfile, 'r') as weather:
        weather_reader = weather.readline()
        while weather_reader != "":
            weather_reader = weather.readline()
            weather_columns = weather_reader.split()
            if weather_columns:
                temperature_diff = int(weather_columns[1].replace("*", "")[:2]) -\
                               int(weather_columns[2].replace("*", "")[:2])
                if temperature_diff > minimum_diff:
                    continue
                else:
                    minimum_diff = temperature_diff
                    index_of_min_temperature = weather_columns[0]

        print(index_of_min_temperature)

weather_calculation('weather.dat')
