Project: DATA MUNGING
​
Description :
​
1. Code written using SOLID principles.
2. Separate Files for weather and football.
3. Breakdown of methods for each purpose.
4. Use of Single Responsibility Principle.
5. Use of Open Closed Principle.
6. Calculate the minimum temperature for the day.
7. Calculate minimum goal difference between 'goals scored for'
   and 'goals scored against' team


Modules:
​
1. dat_file_reader.py: This file read the dat file of weather and football and return the list of lists.
​
2. data_cleaner.py: This file cleans the superfluous data of weather and football columns and return the clean data.
​
3. calculation_difference.py: This file calculate the temperature difference  as well as the goal difference the        return the result.

4. execute.py: This file execute the weather file as well as football file.
​
​
.  Specification: Python 3.7
.  IDE: VS Code
.  For Naming: Used Snake-Case